import os
import pandas as pd
import numpy as np
from plotly import graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px

CLIMB_RATE_MIN =  1.5  #1.016 # m/s

## Define torque ranges of data to slice
LOW_TORQUE_UPPER = 0.7
LOW_TORQUE_LOWER = 0.2

HIGH_TORQUE_UPPER = 1
HIGH_TORQUE_LOWER = 0.


test_set_config_dictionary = {
    "20210611_whole" : {'mass': "4500"},
    "20210624_whole" : {'mass':  "4500"},
    "20210701_whole" : {'mass': "4500"},
    "20210707_whole" : {'mass':  "5110"},
    "20210715_whole" : {'mass':  "5500"},
    "20210720_whole" : {'mass':  "4500"}, 
    "20210722_whole" : {'mass':  "4500"},
}

def import_resample_and_label_time(path, usecols, resample_width):

    df = pd.read_csv(path, usecols=usecols)

    ## Convert timestrings under column 'Timestamp' to datetime64 under 'Datetime' and seconds elapsed under 'Elapsed'
    df['Datetime'] = pd.to_datetime(df['Timestamp'], format='%H:%M:%S:%f')
    df.set_index('Datetime', inplace=True)

    ## Resample data to a 1s period using the (mean) of each period
    period_string = str(resample_width) + 's'
    df = df.resample(period_string).mean()
    df['Elapsed'] = (df.index - pd.Timestamp("1970-01-01")) // pd.Timedelta('1ns')
    df['Elapsed'] = (df['Elapsed'] - df['Elapsed'].iloc[0]) / 10**9

    return ( df )
    

def climb_points(path,set) -> pd.DataFrame:

    """Find and return a dataframe containing Climb data for this flight.
    Args:
        path (str): path to flight test dataframe
    Returns:
        pd.DataFrame: dataframe containing Climb points for this flight
    """

    # variable setup


    index = ['Timestamp']

    ## Pull in only the necessary and relevant variables from the CSVs to save on compute time


    velocities = ['True Airspeed', 'Airspeed', 'Ground Speed', 'Climb Rate', 'Angle of Attack']
    positions = ['GPS Altitude']
    angles = ['ECU Roll Angle', 'ECU Pitch Angle', 'ECU Yaw Angle']
    latlong = ['Latitude', 'Longitude']
    epu = ['Pusher EPU A Commanded Torque' , 'Pusher EPU B Commanded Torque']


    ## Define sets of the variables depending on where they are going


    usecols = index + velocities + positions + angles + latlong + epu

    differentiables = velocities + positions + angles


    ## Define all usable dataframes:


    storage_columns = ["Date","Average Climb Rate of Point","Average Torque of Point","Average Indicated Airspeed"]

    low_torque_climb_data = pd.DataFrame(columns = storage_columns)

    high_torque_climb_data = pd.DataFrame(columns = storage_columns)

    climb_data = pd.DataFrame(columns = storage_columns)


    ## Read in CSV to DataFrame


    RESAMPLE_WIDTH = 1  ## INTEGER SECONDS
    df = import_resample_and_label_time(path, usecols, RESAMPLE_WIDTH)


    ## Run a simple differentiation on the column list (differentiables)
    for column in differentiables:
        df[column + "_differential"] = (df[column].diff(periods=1) / df['Elapsed'].diff(periods=1))


    ## FILTER FOR SECONDS OF DATA WE ARE INTERESTED IN

    climb_rate_lower = df['Climb Rate'] > CLIMB_RATE_MIN

    low_torque_upper = df['Climb Rate'] < LOW_TORQUE_UPPER
    low_torque_lower = df['Climb Rate'] > LOW_TORQUE_LOWER
    
    high_torque_upper = df['Pusher EPU A Commanded Torque'] > HIGH_TORQUE_UPPER
    high_torque_lower = df['Pusher EPU A Commanded Torque'] < HIGH_TORQUE_LOWER

    ## SET AND APPLY FILTERS TO DATAFRAME

    climb_points = df[ (climb_rate_lower) & (low_torque_lower) ] # & (high_torque_upper) & (high_torque_lower)]
    climb_points['Time Difference'] = climb_points['Elapsed'].diff()

    ### Here is the meat. This is where the function searches and pulls individual time discreet points from the csv. 
    ### Using a time difference criteria we can find where the filters chop the data and therefore a simple for loop with an if statement can 
    ### identify these breaks in the data.

    climb_rate_list = []
    seconds_elapsed_list = []
    torque_commanded_list = []
    air_density_list = []
    airspeed_list = []

    average_climbrate_list = []
    average_torque_list = []
    average_air_density_list = []
    average_airspeed_list = []
    average_date_list = []
    average_weight_list = []
    average_density_altitude_list = []

    WEIGHT = test_set_config_dictionary[set]['mass']

    for index, row in climb_points.iterrows():
        
        ## RECORD ALL THE DATA IN ONE DISCREET SECTION
        if row['Time Difference'] <= 2 or (len(seconds_elapsed_list) < 20):

            climb_rate_list.append(row["Climb Rate"] * 196.85)
            torque_commanded_list.append( -1 * 2 *row['Pusher EPU A Commanded Torque'] / 1000)
            air_density_list.append( 1.225 / ((row['True Airspeed'] / row["Airspeed"])**2) )
            seconds_elapsed_list.append(row['Elapsed'])
            airspeed_list.append(row["Airspeed"])


        ## IF A TIME DIFFERENCE BIG ENOUGH IS FOUND, STOP RECORDING ... STOP COLLECTING AND TAKE AN AVERAGE
        if ((row['Time Difference']) > 3) or (len(seconds_elapsed_list) > 20):
            if (len(seconds_elapsed_list) > 20):
                average_climbrate_list.append(np.average(climb_rate_list))
                average_torque_list.append(np.average(torque_commanded_list))
                average_air_density_list.append(np.average(air_density_list))
                average_airspeed_list.append(np.average(airspeed_list))
                average_date_list.append(set)
                average_weight_list.append(WEIGHT)
                average_density_altitude_list.append((-31204*np.average(air_density_list))+38074)

                ## RESET POINT LISTS
                climb_rate_list = []
                torque_commanded_list = []
                seconds_elapsed_list = []
                air_density_list = []
                airspeed_list = []


    climb_data["Date"] = average_date_list
    climb_data["Weight"] = average_weight_list
    climb_data["Average Climb Rate of Point"] = average_climbrate_list
    climb_data["Average Torque of Point"] = average_torque_list
    climb_data["Average Air Density of Point"] = average_air_density_list
    climb_data["Average Density Altitude"] = average_density_altitude_list
    climb_data["Average Indicated Airspeed"] = average_airspeed_list 
            
    return (climb_points, climb_data)


############
## MAIN
###########

## ADD A SERIES DATA PARSING FUNCTIONS (above) AND FEED ALIA 250 CSV FORMAT FLIGHT TEST DATA INTO IT


csv_folder_path = os.path.dirname(os.path.realpath(__file__)) + "/usable_csvs"
input_file_path = os.path.dirname(os.path.realpath(__file__)) + "/Rate_of_Climb_Calculations_norotors_85_51_torque.csv"

climb_estimates = pd.read_csv(input_file_path)

POUNDS_MASS_4500_LB = 4500
POUNDS_MASS_5100_LB = 5100

print(climb_estimates["Mass (pounds)"])

mass_selection_4500_lb = climb_estimates["Mass (pounds)"] > POUNDS_MASS_4500_LB - 10
mass_selection_5100_lb = climb_estimates["Mass (pounds)"] < POUNDS_MASS_5100_LB + 10

####
climb_estimates_4500_lb_50 = climb_estimates[(mass_selection_4500_lb)]
climb_estimates_5100_lb_50= climb_estimates[(mass_selection_5100_lb)]

# List of files in the csv directory

csv_file_name_list = os.listdir(csv_folder_path)

print( "Found " + str(len(csv_file_name_list)) + " tests")


nameout = "takeoff_and_landing_data.csv"

set_list = []
climb_data_list = []



fig1 = make_subplots(rows=1, cols=1, x_title = "Torque Setting (n-m)/(1000 n-m)" , y_title = "Climbrate (fpm)")
fig2 = make_subplots(rows=1, cols=1, x_title = "Density Altitude (feet)" , y_title = "Climbrate (fpm)")
fig3 = make_subplots(rows=1, cols=1, x_title = "Density Altitude (feet)" , y_title = "Climbrate (fpm)")
fig4 = make_subplots(rows=1, cols=1, x_title = "Density Altitude (feet)" , y_title = "Climbrate (fpm)")

fig1.update_layout(title_text = "Climbrate (fpm) vs  Torque Setting (n-m)/(1000 n-m) - Climbrate + / - " + str(round(CLIMB_RATE_MIN*196.85 )) + " (fpm)")    
fig2.update_layout(title_text = "HIGH TRQ (70 - 100%) Climbrate (fpm) vs Density Altitude (feet) - Climbrate + / - " + str(round(CLIMB_RATE_MIN*196.85 )) + " (fpm)")  
fig3.update_layout(title_text = "LOW TRQ (45 - 65%) Climbrate (fpm) vs Density Altitude (feet) - Climbrate + / - " + str(round(CLIMB_RATE_MIN*196.85 )) + " (fpm)")  
fig4.update_layout(title_text = "FULL RANGE TRQ (45 - 100%) Climbrate (fpm) vs Density Altitude (feet) - Climbrate + / - " + str(round(CLIMB_RATE_MIN*196.85 )) + " (fpm)")  

altitude_200 = np.arange(500 , 6000, 200 )
altitude_200_half_upper = np.arange(4000 , 6000, 200 )
altitude_200_half_lower = np.arange(0 , 4000, 200 )
altitude_2000 = np.arange(0 , 6000, 2000 )

torque = np.arange(0.3 , 1.350 , 0.1 )

for set in csv_file_name_list:

    print("Reading in", set, "..." )

    WEIGHT = test_set_config_dictionary[set]['mass']
    # obstacle_clearance_points_output = obstacle_clearance_points(pattern_points_output[0])
    climb_points_output = climb_points(csv_folder_path + "/" +set,set)
    
    climb_data_list.append(climb_points_output[1])
    climb_data = climb_points_output[1]

    low_torque_upper = climb_data["Average Torque of Point"] < LOW_TORQUE_UPPER
    low_torque_lower = climb_data["Average Torque of Point"] > LOW_TORQUE_LOWER
    
    high_torque_upper = climb_data["Average Torque of Point"] < HIGH_TORQUE_UPPER
    high_torque_lower = climb_data["Average Torque of Point"] > HIGH_TORQUE_LOWER

    low_torque_climb_data = climb_data[ (low_torque_upper) & (low_torque_lower) ]
    high_torque_climb_data = climb_data[ (high_torque_upper) & (high_torque_lower) ]

    print(low_torque_climb_data["Average Torque of Point"])



    

    
  


    low_alt = high_torque_climb_data["Average Density Altitude"] < 4000
    high_alt = high_torque_climb_data["Average Density Altitude"] > 4000

    high_torque_climb_data_lower = high_torque_climb_data[(low_alt)] 
    high_torque_climb_data_upper = high_torque_climb_data[(high_alt)] 

    print(high_torque_climb_data_lower["Average Density Altitude"] )


    z1 = np.polyfit(climb_data["Average Torque of Point"], climb_data["Average Climb Rate of Point"], 1) # Polynomial fitt
    p1 = np.poly1d(z1)  

    fig1.add_trace(go.Scatter(x=climb_data["Average Torque of Point"], y=climb_data["Average Climb Rate of Point"], mode='markers', name = WEIGHT + " " + set[:8] ), row=1, col=1)
    fig1.add_trace(go.Line(x = torque, y=p1(torque) , mode = 'lines', name = WEIGHT + " " + set[:8]), row=1, col=1)

    ## HIGH TRQ DENSITY VS ROC
    ## High density altitude polyfit
    z5 = np.polyfit(high_torque_climb_data_lower["Average Density Altitude"] , high_torque_climb_data_lower["Average Climb Rate of Point"], 2) # Polynomial fit
    p5 = np.poly1d(z5)
    
    ## Low density altitude polyfit
    z6 = np.polyfit(high_torque_climb_data_upper["Average Density Altitude"] , high_torque_climb_data_upper["Average Climb Rate of Point"], 2) # Polynomial fit
    p6 = np.poly1d(z6)

    fig2.add_trace(go.Scatter(x=high_torque_climb_data["Average Density Altitude"], y=high_torque_climb_data["Average Climb Rate of Point"],mode = 'markers' , name =  WEIGHT + " " + set[:8] ), row=1, col=1)
    fig2.add_trace(go.Line(x = altitude_200_half_upper, y=p6(altitude_200_half_upper) , mode = 'lines', name = WEIGHT + " " + set[:8] ), row=1, col=1)
    fig2.add_trace(go.Line(x = altitude_200_half_lower, y=p5(altitude_200_half_lower) , mode = 'lines', name = WEIGHT + " " + set[:8] ), row=1, col=1)

    z3 = np.polyfit(low_torque_climb_data["Average Density Altitude"], low_torque_climb_data["Average Climb Rate of Point"], 2) # Polynomial fit
    p3 = np.poly1d(z3)

    fig3.add_trace(go.Scatter(x=low_torque_climb_data["Average Density Altitude"], y=low_torque_climb_data["Average Climb Rate of Point"],mode = 'markers' , name = WEIGHT + " "  + set[:8] ), row=1, col=1)
    fig3.add_trace(go.Line(x = altitude_200, y=p3(altitude_200) , mode = 'lines', name = WEIGHT + " " + set[:8] ), row=1, col=1)

    z4 = np.polyfit(climb_data["Average Density Altitude"], climb_data["Average Climb Rate of Point"], 2) # Polynomial fit
    p4 = np.poly1d(z3)
    fig4.add_trace(go.Scatter(x=climb_data["Average Density Altitude"], y=climb_data["Average Climb Rate of Point"],mode = 'markers' , name = WEIGHT + " " + set[:8] ), row=1, col=1)
    fig4.add_trace(go.Line(x = altitude_200, y=p4(altitude_200) , mode = 'lines', name = WEIGHT + " " + set[:8] ), row=1, col=1)

# mode='lines+markers'
fig1.show()
fig2.show()
fig3.show()
fig4.show()
climb_data = pd.concat(climb_data_list)

climb_points_output[0]['Climb Rate'] = climb_points_output[0]['Climb Rate'].mul(196.85)


fig5 = make_subplots(rows=4, cols=1)
fig5.add_trace(go.Scatter(x=climb_points_output[0]['Elapsed'], y=climb_points_output[0]['Climb Rate'], mode='markers', name='Climbrate (fpm)'), row=1, col=1)
fig5.add_trace(go.Scatter(x=climb_points_output[0]['Elapsed'], y=climb_points_output[0]['GPS Altitude'], mode='markers', name='Altitude (feet)'), row=2, col=1)
fig5.add_trace(go.Scatter(x=climb_points_output[0]['Elapsed'], y=climb_points_output[0]['True Airspeed'], mode='markers', name='True Airspeed (knots)'), row=3, col=1)
fig5.add_trace(go.Line(x=climb_points_output[0]['Elapsed'], y=(-1*climb_points_output[0]['Pusher EPU A Commanded Torque']+-1*climb_points_output[0]['Pusher EPU B Commanded Torque']), mode='markers', name='Torque (n-m)'), row=4, col=1)

fig5.show()

# fig6 = make_subplots(rows=1, cols=1)
# fig6.add_trace(go.Scatter(x=low_torque_climb_rates_total["Average Air Density of Point"], y=low_torque_climb_rates_total["Average Climb Rate of Point"], mode='markers', name='Low Torque Climbs', color = low_torque_climb_rates_total["Date"]), row=1, col=1)
# fig6.add_trace(go.Scatter(x=high_torque_climb_rates_total["Average Air Density of Point"], y=high_torque_climb_rates_total["Average Climb Rate of Point"], mode='markers', name='High Torque Climbs',color =  high_torque_climb_rates_total["Date"]), row=1, col=1)
# fig6.show()

output_dataframe = pd.DataFrame()

set_list.append(set)





