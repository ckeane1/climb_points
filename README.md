## Project Purpose:
Sorting python based functions for flight data analysis. 

These functions intake the ALIA 250 CSV format and output desired plots and csv files. 

## Installation:

Create a folder in the root directory of this file called "usable_csvs".

This also happens to be the output folder of the gen_csv() function. See more about pairing these two here: (WIP)

pip install os
pip install pandas 
pip install numpy
pip install plotly.express

## Change directory to the root of the python file.

back out a file layer with 
cd ../

enter a file layer
cd root_directory.py

## Run with:

"python climb_points.py"
